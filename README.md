Full Site Federation eCommerce Example
===============================================

An example eCommerce app Module Federation in a Full Site Federation configuration, using [react-router-dom](https://www.npmjs.com/package/react-router-dom) to manage the routing.

- Video Reference [link](youtube.com/watch?v=lKKsjpH09dU)
- Repository [link](https://github.com/jherr/micro-fes-beginner-to-expert)

# Installation

In these five directories; `addtocart`, `cart`, `home`, `pdp` and `server` run these commands:

```sh
yarn && yarn start
```

In a different terminal window for each app.

The visit the [home page](http://localhost:3000/).

===============================================

### Main Learnings

- **TRICK** Wrapping a microfrontend component with **SafeComponents** (./pdp/src/SafeComponent.jsx)
R: To avoid break the whole app when the microfrontend change. [Error Handling](https://www.youtube.com/watch?v=lKKsjpH09dU&t=1496s)

### Observations

- Video checkpoint [link](https://www.youtube.com/watch?v=lKKsjpH09dU&t=1902s)